<?php
/**
 * Created by PhpStorm.
 * User: JuriPetersen
 * Date: 30.01.18
 * Time: 10:26
 */

namespace Ean2asin\Providers;

use Plenty\Plugin\ServiceProvider;

class Ean2asinServiceProvider extends ServiceProvider
{
    public function register(){
        $this->getApplication()->register(Ean2asinRouteServiceProvider::class);
    }
}