<?php
/**
 * Created by PhpStorm.
 * User: JuriPetersen
 * Date: 30.01.18
 * Time: 10:21
 */
namespace Ean2asin\Providers;

use Plenty\Plugin\RouteServiceProvider;
use Plenty\Plugin\Routing\Router;
use Plenty\Plugin\Routing\ApiRouter;

class Ean2asinRouteServiceProvider extends RouteServiceProvider
{
    public function map(ApiRouter $apiRouter,Router $router){
        $apiRouter->version(['v1'],['namespace' => 'Ean2asin\Controllers' , 'middleware' => 'oauth'],
            function($apiRouter){
                $apiRouter->get('Ean2asin/test', 'TestController@test');
            });


        $router->get('Ean2asin/run', 'Ean2asin\Controllers\TestController@test');
    }

}