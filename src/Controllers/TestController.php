<?php
/**
 * Created by PhpStorm.
 * User: JuriPetersen
 * Date: 30.01.18
 * Time: 10:20
 */
namespace Ean2asin\Controllers;

use Plenty\Plugin\Controller;

class TestController extends Controller
{
    public function test(){
        $test = 'lul test';
        return json_encode($test);
    }
}