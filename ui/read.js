var token = localStorage.getItem("accessToken");
var url = "/Ean2asin/run";


$( document ).ready( $.ajax({
    url: url,
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token);
    },
    type: 'GET',
    dataType: 'json',
    contentType: 'application/json',
    processData: false,
    success: function (data) {
        $("#error_msg_display").html("<strong style='color:#ff0000;'>"+data+"</strong>");
    },
    error: function(){
        $("#error_msg_display").html("<strong style='color:#ff0000;'>Achtung! <br> Es entstand ein Fehler beim Einlesen der Daten</strong>");
    }
}));

